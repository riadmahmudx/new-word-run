﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class PathController : MonoBehaviour
{
    public GameObject[] paths;
    public GameObject fillerPrefab;
    public int wordLength = 0;
    Rigidbody rb;
   



    private float pathHorizontalLength;

    void Start()
    {
        pathHorizontalLength = paths[0].GetComponent<Renderer>().bounds.size.x;
    }


    void Update()
    {

        if (paths[0].transform.position.x < - pathHorizontalLength)
        {
            RepositionPath(0);
        }
        if (paths[1].transform.position.x < - pathHorizontalLength)
        {
            RepositionPath(1);
        }
        if (paths[2].transform.position.x < - pathHorizontalLength)
        {
            RepositionPath(2);
        }

    }

    void RepositionPath(int x)

    {
        if (x == 0)
        {
            Vector3 pathOffset = new Vector3(pathHorizontalLength + (float)wordLength , 0, 0);

            paths[x].transform.position = paths[2].transform.position + pathOffset;

            for (int i = 1; i <= wordLength; i++)
            {
                Vector3 fillerOffset = new Vector3(paths[2].transform.position.x + pathHorizontalLength / 2 + 0.5f + ((float)i - 1), -2.5f-i, 0);
                Instantiate(fillerPrefab, fillerOffset, Quaternion.identity);
            }

            wordLength = 0;
        }
        else if (x == 1)
        {
            Vector3 pathOffset = new Vector3(pathHorizontalLength + (float)wordLength, 0, 0);

            paths[x].transform.position = paths[0].transform.position + pathOffset;

            for (int i = 1; i <= wordLength; i++)
            {
                Vector3 fillerOffset = new Vector3(paths[0].transform.position.x + pathHorizontalLength / 2 + 0.5f + ((float)i - 1), -2.5f-i, 0);
                Instantiate(fillerPrefab, fillerOffset, Quaternion.identity);
            }

            wordLength = 0;

        }
        else if (x == 2)
        {
            Vector3 pathOffset = new Vector3(pathHorizontalLength + (float)wordLength, 0, 0);

            paths[x].transform.position = paths[1].transform.position + pathOffset;

            for (int i = 1; i <= wordLength; i++)
            {
                Vector3 fillerOffset = new Vector3(paths[1].transform.position.x + pathHorizontalLength / 2 + 0.5f + ((float)i - 1), -2.5f-i, 0);
                Instantiate(fillerPrefab, fillerOffset, Quaternion.identity);
            }

            wordLength = 0;
        }
    }


    //public void GoUpFiller()

    //{
        
    //    string fillerName = "Filler(Clone)";
    //    GameObject[] fillerPresntAtScene = FindGameObjectsWithName(fillerName);
     

    //    for (int i = 0; i < fillerPresntAtScene.Length; i++)
    //    {
            
    //        rb = fillerPresntAtScene[i].GetComponent<Rigidbody>();

    //        Vector3 localVelocity = transform.InverseTransformDirection(rb.velocity);
    //        localVelocity.y = GameControl.instance.scrollSpeed + 2.5f;
    //        rb.velocity = transform.TransformDirection(localVelocity);
           

          
    //    }

    //}



     //GameObject[] FindGameObjectsWithName(string nameIt)
         //{
         //    int it = 0;
         //    GameObject[] objArr;
         //    bool b = false;
         //    while (!b) 
         //    {
         //        if(GameObject.Find(nameIt))
         //        {
         //            GameObject.Find(nameIt).name = nameIt + it;
         //            it++;
         //        }
         //        else
         //        {
         //            b=true; 
         //        }
         //    }
     
         //    objArr = new GameObject[it];
         //    while (it > 0) 
         //    {
         //        it--;
         //        objArr[it] = GameObject.Find(nameIt + it);
         //        objArr[it].name = nameIt;
         //    }
     
         //    return objArr;
         //}//END FindGameObjectsWithName 
   
}
