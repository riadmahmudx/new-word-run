﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillerDestroyer : MonoBehaviour
{
    public GameObject DustParticle;
    Rigidbody rb;
   

    void Start()
     
    {
        rb= gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
       
        if(GameControl.instance.isFillerTrue == true)
        {
            rb.velocity = new Vector3(0f, GameControl.instance.scrollSpeed + 2.5f , 0f);
         //   Debug.Log(isFillerTrue);
        }

        if(transform.position.y>=-0.01f)
        {
            
            GameControl.instance.isFillerTrue = false;
            //rb.velocity = new Vector3(0f, 0f, 0f);
           
            transform.position = new Vector3 (transform.position.x , 0,transform.position.z);
            DustParticle.SetActive(true);
        }

        if (transform.position.x < - 20f)
        {
            Destroy(gameObject);
        } 
    }

   

}
