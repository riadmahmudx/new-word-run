﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    public GameObject currentHitObject;
    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;


    private Vector3 origin;
    private Vector3 direction;

    private float currentHitDistance;

    public Animator ballAnimator;

    void Start()
    {
        
    }


    void Update()
    {
        origin = transform.position;
        direction = -transform.up;
        RaycastHit hit;

        if(Physics.SphereCast(origin,sphereRadius,direction, out hit,maxDistance,layerMask, QueryTriggerInteraction.UseGlobal))

        {
            currentHitObject = hit.transform.gameObject;
            currentHitDistance = hit.distance;
        }
        else 
        {
           
            currentHitDistance = maxDistance;
            currentHitObject = null;
            ballAnimator.SetTrigger("gameover");
            GameControl.instance.scrollSpeed = Mathf.Lerp(GameControl.instance.scrollSpeed, 0 , 0.1f);



        }
        
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Debug.DrawLine(origin, origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
    }
}
