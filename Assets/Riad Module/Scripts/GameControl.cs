﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    public static GameControl instance;
    public float scrollSpeed;
    public bool isFillerTrue = false;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
    }

   
    void Update()
    {
        
    }


    public void StartScroll()
    {
       scrollSpeed = 5f;
    }

    public void StopScroll()
    {
       scrollSpeed = 0f;
    }

    public void GoUp()
    {
        isFillerTrue = true;


    }

}
