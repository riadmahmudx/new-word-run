﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DragAndDropRaycast : MonoBehaviour
{
    public bool isClickEnabled = false;
    const string contentTag = "content";
    public Camera cam;
    GameObject selectedItem;
    Vector3 startLocaton;
    public string selectedString;
    public List<TextMeshProUGUI> textList;
    bool isStartMoving;

    void Awake()
    {
        cam = Camera.main;
        textList = new List<TextMeshProUGUI>();
        GetChildren();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isClickEnabled && selectedItem == null)
            {
                RaycastHit hit;
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider != null && hit.transform.CompareTag(contentTag))
                    {
                        hit.collider.enabled = false;
                        startLocaton = hit.transform.localPosition;
                        selectedItem = hit.transform.gameObject;
                        Debug.Log(startLocaton + " PRev");
                    }
                }
            }

        }

        if (Input.GetMouseButton(0))
        {
            if (selectedItem != null)
            {
                MoveToLocation(selectedItem);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (selectedItem != null)
            {
                CheckForOtherContent();
            }
        }

        if (isStartMoving)
        {
            transform.Translate(Vector3.down * Time.deltaTime);
        }
    }

    public void StartMovement()
    {
        isStartMoving = true;
    }
    public void MoveToLocation(GameObject cube)
    {
        selectedItem.transform.position = new Vector3(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y, 0); //cam.ScreenToWorldPoint(Input.mousePosition).z

    }
    public void MovetoStartLocation()
    {
        Debug.LogError(startLocaton + " MOVE ");
        selectedItem.GetComponent<Collider>().enabled = true;
        selectedItem.transform.localPosition = startLocaton;
    }

    public void CheckForOtherContent()
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {

            if (hit.collider != null && hit.transform.CompareTag(contentTag))
            {
                selectedItem.gameObject.SetActive(false);
                hit.transform.gameObject.SetActive(false);

                selectedItem.transform.position = hit.transform.position;
                hit.transform.localPosition = new Vector3(startLocaton.x, 0, 0);
                selectedItem.GetComponent<BoxCollider>().enabled = true;

                selectedItem.gameObject.SetActive(true);
                hit.transform.gameObject.SetActive(true);

                CheckMatchString();
            }
            else
            {
                MovetoStartLocation();
            }
        }
        selectedItem = null;
    }

    void CheckMatchString()
    {
        string str = null;
        string[] tempList = new string[textList.Count];

        PosWithLetter[] tempArray = new PosWithLetter[textList.Count];

        for (int i = 0; i < textList.Count; i++)
        {
            PosWithLetter temp = new PosWithLetter();
            temp.position = textList[i].transform.position.x;
            temp.letter = textList[i].text;

            tempArray[i] = temp;
        }

        for (int i = 0; i < textList.Count; i++)
        {
            for (int j = 0; j < textList.Count - 1; j++)
            {
                if (tempArray[j].position > tempArray[j + 1].position)
                {
                    PosWithLetter temp = tempArray[j + 1];
                    tempArray[j + 1] = tempArray[j];
                    tempArray[j] = temp;
                }
            }
        }

        for (int i = 0; i < tempArray.Length; i++)
        {
            str += tempArray[i].letter;
        }
        Debug.LogError(str);
        if (selectedString == str)
        {
            Debug.LogError("Its matched");
            Destroy(this.gameObject);
        }
        else
        {
            Debug.LogError("Not ");
        }
    }

    List<char> GetSplitCharacter(string givenString)
    {
        List<char> tempList = new List<char>();

        foreach (var item in givenString)
        {
            tempList.Add(item);
        }

        return tempList;
    }

    public void GetChildren()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            textList.Add(transform.GetChild(i).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>());
        }
    }

    public void SetData(string str)
    {
        selectedString = str;
        List<char> charList = GetSplitCharacter(selectedString);

        Shuffle(charList);
        SetCharacterToObject(charList);
    }

    private void SetCharacterToObject(List<char> charList)
    {
        for (int i = 0; i < charList.Count; i++)
        {
            textList[i].text = charList[i].ToString();
        }
    }

    void Shuffle(List<char> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            int randomIndex = UnityEngine.Random.Range(i, list.Count);
            char temp = list[i];
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }
}

public struct PosWithLetter
{
    public float position;
    public string letter;
}