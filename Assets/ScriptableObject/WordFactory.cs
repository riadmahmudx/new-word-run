﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "WordFactoryData", menuName = "WordFactory/WordFactoryData", order = 0)]
public class WordFactory : ScriptableObject
{
    public List<WordSturct> wordFactoryList;
}


[System.Serializable]
public struct WordSturct
{
    public int level;
    public List<string> wordList;
}